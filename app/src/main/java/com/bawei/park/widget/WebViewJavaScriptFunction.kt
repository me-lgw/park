package com.bawei.park.widget

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 10:11
 * Description: This is WebViewJavaScriptFunction
 */
interface WebViewJavaScriptFunction {
    fun onJsFunctionCalled(tag:String?)
}