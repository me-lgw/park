package com.bawei.park.widget

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import com.tencent.smtt.sdk.WebSettings
import com.tencent.smtt.sdk.WebView
import com.tencent.smtt.sdk.WebViewClient

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 10:13
 * Description: This is X5WebView
 */
class X5WebView : WebView {

    var title: TextView? = null
    private val client: WebViewClient = object : WebViewClient() {
        /**
         * 防止加载网页时调起系统浏览器
         */
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    constructor(arg0: Context?, arg1: AttributeSet?) : super(arg0, arg1) {
        this.webViewClient = client
        initWebViewSettings()
        this.view.isClickable = true
    }

    private fun initWebViewSettings() {
        val webSetting = this.settings
        webSetting.javaScriptEnabled = true
        webSetting.javaScriptCanOpenWindowsAutomatically = true
        webSetting.allowFileAccess = true
        webSetting.layoutAlgorithm = WebSettings.LayoutAlgorithm.NARROW_COLUMNS
        webSetting.setSupportZoom(true)
        webSetting.builtInZoomControls = true
        webSetting.useWideViewPort = true
        webSetting.setSupportMultipleWindows(true)
        webSetting.setAppCacheEnabled(true)
        webSetting.domStorageEnabled = true
        webSetting.setGeolocationEnabled(true)
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE)
        webSetting.pluginState = WebSettings.PluginState.ON_DEMAND
        webSetting.cacheMode = WebSettings.LOAD_NO_CACHE
    }

    override fun drawChild(canvas: Canvas, child: View, drawingTime: Long): Boolean {
        val ret = super.drawChild(canvas, child, drawingTime)
//        canvas.save()
//        val paint = Paint()
//        paint.color = 0x7fff0000
//        paint.textSize = 24f
//        paint.isAntiAlias = true
//        if (x5WebViewExtension != null) {
//            canvas.drawText(this.context.packageName + "-pid:"
//                    + Process.myPid(), 10f, 50f, paint)
//            canvas.drawText(
//                    "X5  Core:" + QbSdk.getTbsVersion(this.context) + " sdk " + QbSdk.getTbsSdkVersion(), 10f, 100f, paint)
//        } else {
//            canvas.drawText(this.context.packageName + "-pid:"
//                    + Process.myPid(), 10f, 50f, paint)
//            canvas.drawText("Sys Core" + " sdk " + QbSdk.getTbsSdkVersion(), 10f, 100f, paint)
//        }
//        canvas.drawText(Build.MANUFACTURER, 10f, 150f, paint)
//        canvas.drawText(Build.MODEL, 10f, 200f, paint)
//        canvas.restore()
        return super.drawChild(canvas, child, drawingTime)
    }

    constructor(arg0: Context?) : super(arg0) {
        setBackgroundColor(85621)
    }
}