package com.bawei.park.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mooc.lib_common.action.BusAction
import com.mooc.lib_common.action.BusConst
import com.mooc.lib_common.bus.LiveDataBus
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 14:02
 * Description: This is SplashViewModel
 */
class SplashViewModel : ViewModel() {

    val startPagerData = LiveDataBus<BusAction>()

    fun startTimmer() {
        viewModelScope.launch {
            delay(1000)
            startPagerData.postValue(BusAction(BusConst.LOGINACTION,null))
        }
    }
}