package com.bawei.park.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;

import androidx.appcompat.app.AppCompatActivity;
import com.bawei.park.R;

import com.bawei.park.ui.parkingall.ParkingAllActivity;
import com.bawei.park.widget.WebViewJavaScriptFunction;
import com.bawei.park.widget.X5WebView;
import com.blankj.utilcode.util.LogUtils;

import org.jetbrains.annotations.Nullable;

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 10:07
 * Description: This is MainActivity
 */
public class MainActivity extends AppCompatActivity implements WebViewJavaScriptFunction {
    private X5WebView x5WebView;

    @Override
    protected void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        x5WebView = findViewById(R.id.main_x5web);
        x5WebView.addJavascriptInterface(this,"androidinfo");
        x5WebView.loadUrl("http://118.195.161.134:8077/index.html");
    }

    @Override
    public void onJsFunctionCalled(@Nullable String tag) {

    }



    //封装界面跳转方法
//    private void start(Class clz){
//        Intent intent = new Intent(this,clz);
//        startActivity(intent);
//    }

    //注册要监听的js事件
    @JavascriptInterface
    public void androidparking(String id){
        startActivity(new Intent(this,ParkingAllActivity.class));
    }

    @JavascriptInterface
    public void androidapply(String id){
    }

    @JavascriptInterface
    public void androidvisit(String id){
    }

    @JavascriptInterface
    public void androidpatrol(String id){
    }

    @JavascriptInterface
    public void androidnews(String id){
    }

    @JavascriptInterface
    public void androidnoticelist(String id){
    }

    @JavascriptInterface
    public void androidrepair(String id){
    }

    @JavascriptInterface
    public void androidaddrepair(String id){
    }

    @JavascriptInterface
    public void androidaddculture(String id){
        //添加文化
    }

    @JavascriptInterface
    public void androidaddnotice(String id){
        //发公告
    }

    @JavascriptInterface
    public void androidculture(String id){
        //文化审核
    }

    @JavascriptInterface
    public void androidnotice(String id){
        //公告管理
    }

    @JavascriptInterface
    public void androidsign(String id){
    }

    @JavascriptInterface
    public void androidattendance(String id){
    }

    @JavascriptInterface
    public void androidproperty(String id){
    }

    @JavascriptInterface
    public void androidcheckculture(String id){
        //文化审核
    }

    @JavascriptInterface
    public void androidpeople(String id){
    }

}
