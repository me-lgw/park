package com.bawei.park.ui.apply

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/7
 * Time: 9:33
 * Description: This is ApplyEntity
 */
class ApplyEntity {
    var type = 0
    var state = 0
    var applyTime: String? = null
    var title: String? = null
    var visitorId = 0
    var parkId = 0
    var repairId = 0
    var color = 0
}