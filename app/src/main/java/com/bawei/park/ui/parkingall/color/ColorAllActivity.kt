package com.bawei.park.ui.parkingall.color

import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.bawei.park.R
import com.bawei.park.databinding.ActivityColorallBinding
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemChildClickListener
import com.mooc.lib_common.mvvm.BaseActivity
import kotlinx.android.synthetic.main.activity_colorall.*

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/3
 * Time: 9:11
 * Description: This is ColorAllActivity
 */
//全部颜色
class ColorAllActivity : BaseActivity<ColorAllViewModel,ActivityColorallBinding>(),
    OnItemChildClickListener {
    val colorAllAdaper : ColorAllAdaper = ColorAllAdaper()
    override fun initView() {
        var manager = GridLayoutManager(this, 1)
        manager.orientation = GridLayoutManager.HORIZONTAL
        v.recColorAll.layoutManager = manager
        v.recColorAll.adapter = colorAllAdaper
        colorAllAdaper.setOnItemChildClickListener(this)
    }

    override fun bindLayout(): Int {
        return R.layout.activity_colorall
    }

    override fun initData() {
        vm.data.observe(this,{
            colorAllAdaper.setNewInstance(it)
        })
        vm.requestColor()
    }

    override fun onItemChildClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
        val data = vm.data
        color_item.setBackgroundColor(Color.parseColor(data.value!![position].colorValue))
    }
}