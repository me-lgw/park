package com.bawei.park.ui.login

import android.text.TextUtils
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.blankj.utilcode.util.SPUtils
//import com.blankj.utilcode.util.ToastUtils
import com.mooc.lib_common.bus.LiveDataBus
import com.mooc.lib_network.entity.NetResult
import kotlinx.coroutines.launch

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 13:03
 * Description: This is LoginViewModel
 */
class LoginViewModel : ViewModel() {
    val loginRepo = LoginRepo()
    val name = ObservableField<String>()
    val pwd = ObservableField<String>()
    val loginBus = LiveDataBus<String>()

    fun onLoginClick() {
        if (TextUtils.isEmpty(name.get())){
//            ToastUtils.showShort("请输入用户名")
            return
        }
        if (TextUtils.isEmpty(pwd.get())){
//            ToastUtils.showShort("请输入密码")
            return
        }
        viewModelScope.launch {
            var requestStr = "{\"uName\":\""+name.get()+"\"," +
                    "\"uPwd\":\""+pwd.get()+"\"," +
                    "\"time\":\""+(System.currentTimeMillis()/1000)+"\"}"
            val result = loginRepo.login(requestStr)
            if (result is NetResult.Success){
                //成功
                loginBus.postValue("http://118.195.161.134:8088/index.html")
                SPUtils.getInstance().put("token",result.data.token)
                Log.e("lgw","NetResult.Success"+result.data.token)
                //eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI0NyIsImV4cCI6MzI5MjU4ODExOCwiaWF0IjoxNjQ2Mjk0MDU5fQ.lJWS6oAz69ScalWFZtunUqnN--PHATujsfMm21-v8wk
            }else{
                Log.e("lgw","NetResult.Erro")
            }
        }
    }
}