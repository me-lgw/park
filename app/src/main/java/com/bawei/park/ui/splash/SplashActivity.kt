package com.bawei.park.ui.splash

import android.content.Intent
import androidx.lifecycle.Observer
import com.bawei.park.R
import com.bawei.park.databinding.ActivitySplashBinding
import com.bawei.park.ui.login.LoginActivity
import com.mooc.lib_common.action.BusAction
import com.mooc.lib_common.action.BusConst
import com.mooc.lib_common.mvvm.BaseActivity

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 14:34
 * Description: This is SplashActivity
 */
class SplashActivity : BaseActivity<SplashViewModel,ActivitySplashBinding>(),Observer<BusAction> {

    override fun initView() {

    }

    override fun bindLayout(): Int {
        return R.layout.activity_splash
    }

    override fun initData() {
        vm.startPagerData.observe(this, this)
        vm.startTimmer()
    }

    override fun onChanged(t: BusAction?) {

        if (t!!.action.equals(BusConst.LOGINACTION)) {
            //定时完成，用户信息获取切换界面判断
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }
}