package com.bawei.park.ui.parkingall

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mooc.lib_common.bus.LiveDataBus
import com.mooc.lib_network.entity.NetResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/1
 * Time: 18:41
 * Description: This is ParkingAllViewModel
 */
class ParkingAllViewModel : ViewModel() {
    val parkModel = ParkingAllModel()
    val data = LiveDataBus<ArrayList<ParkingAllEntity>>()

    fun requestPark() {
        viewModelScope.launch(Dispatchers.IO) {
            var parkingAll = parkModel.parkingAll()
            if (parkingAll is NetResult.Success) {
                var list = parkingAll.data
                var dataArray = ArrayList<ParkingAllEntity>()
                var index = 0
                var line = 1
                while (index < list.size) {
                    if (index % 5 == 0) {
                        var entity = ParkingAllEntity()
                        entity.parkName = "第"+line+"行"
                        entity.type = 1
                        dataArray.add(entity)
                        line++
                    } else {
                        //非第一列数据
                        //区分是否选中
                        dataArray.add(list[index])
                    }
                    index++
                }

                data.postValue(dataArray)
            }
        }
    }
}