package com.bawei.park.ui.parkingall

import android.content.Context
import com.yanyusong.y_divideritemdecoration.Y_Divider
import com.yanyusong.y_divideritemdecoration.Y_DividerBuilder
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/2
 * Time: 16:23
 * Description: This is ParkDividerItemDecoration
 */
class ParkDividerItemDecoration(content : Context?) : Y_DividerItemDecoration(content) {

    suspend fun ParkDividerItemDecoration(context : Context){
        suspend { context }
    }

    override fun getDivider(itemPosition: Int): Y_Divider? {
        var divider : Y_Divider? = null
        when(itemPosition % 6){
            0,1,2,3,4 ->
                divider = Y_DividerBuilder().
                setRightSideLine(true, 0xffffffff.toInt(), 3F, 0F, 0F).
                setBottomSideLine(true, 0xffffffff.toInt(),3F,0F,0F).
                create()
            5 ->
                divider = Y_DividerBuilder().
                setBottomSideLine(true,0xffffffff.toInt(),3F,0F,0F).
                create()

        }
        return divider
    }
}