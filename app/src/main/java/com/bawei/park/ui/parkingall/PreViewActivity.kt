package com.bawei.park.ui.parkingall

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bawei.park.R
import com.bawei.park.ui.parkingall.color.ColorAllActivity
import com.google.vr.sdk.widgets.pano.VrPanoramaEventListener
import com.google.vr.sdk.widgets.pano.VrPanoramaView
import kotlinx.android.synthetic.main.activity_preview.*
import java.io.IOException
import java.io.InputStream


/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/2
 * Time: 18:56
 * Description: This is PreViewActivity
 */

//展示VR效果
class PreViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)
        vrPre()

        choice.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, ColorAllActivity::class.java))
        })
    }


    fun vrPre() {
        var open: InputStream? = null
        try {
            open = assets.open("cc.jpg")
        } catch (e: IOException) {
            e.printStackTrace()
        }
        val bitmap = BitmapFactory.decodeStream(open)
        /**设置加载VR图片的相关设置**/
        /**设置加载VR图片的相关设置 */
        val options = VrPanoramaView.Options()
        options.inputType = VrPanoramaView.Options.TYPE_STEREO_OVER_UNDER
        /**设置加载VR图片监听**/
        /**设置加载VR图片监听 */
        vr.setEventListener(object : VrPanoramaEventListener() {
            /**
             * 显示模式改变回调
             * 1.默认
             * 2.全屏模式
             * 3.VR观看模式，即横屏分屏模式
             * @param newDisplayMode 模式
             */
            override fun onDisplayModeChanged(newDisplayMode: Int) {
                super.onDisplayModeChanged(newDisplayMode)

            }

            /**
             * 加载VR图片失败回调
             * @param errorMessage
             */
            override fun onLoadError(errorMessage: String) {
                super.onLoadError(errorMessage)

            }

            /**
             * 加载VR图片成功回调
             */
            override fun onLoadSuccess() {
                super.onLoadSuccess()

            }

            /**
             * 点击VR图片回调
             */
            override fun onClick() {
                super.onClick()

            }
        })
        /**加载VR图片**/
        /**加载VR图片 */
        vr.loadImageFromBitmap(bitmap, options)
    }

    companion object {
        private const val TAG = "PreViewActivity"
    }
}