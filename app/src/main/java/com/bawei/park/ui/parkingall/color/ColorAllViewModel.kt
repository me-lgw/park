package com.bawei.park.ui.parkingall.color

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mooc.lib_common.bus.LiveDataBus
import com.mooc.lib_network.entity.NetResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.launch

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/3
 * Time: 16:32
 * Description: This is ColorAllViewModel
 */
class ColorAllViewModel : ViewModel() {

    var colorModel = ColorAllModel()
    var data = LiveDataBus<ArrayList<ColorAllEntity>>()

    fun requestColor() {
        viewModelScope.launch(Dispatchers.IO) {
            var colorall = colorModel.colorAll()
            if (colorall is NetResult.Success) {
                var list = colorall.data
                data.postValue(list)
            }
        }
    }
}