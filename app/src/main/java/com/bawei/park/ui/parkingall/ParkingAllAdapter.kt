package com.bawei.park.ui.parkingall

import android.graphics.Color
import android.widget.TextView
import com.bawei.park.R
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/2
 * Time: 11:13
 * Description: This is ParkingAllAdaoter
 */
class ParkingAllAdapter : BaseQuickAdapter<ParkingAllEntity,
        BaseViewHolder>(R.layout.item_parkingall) {

    init {
        addChildClickViewIds(R.id.tv_parkingAll)
    }

    override fun convert(holder: BaseViewHolder, item: ParkingAllEntity) {
        var tv = holder.getView<TextView>(R.id.tv_parkingAll)
        tv.setTextColor(Color.parseColor("#c1c1c1"))
        if (item.type == 1) {
            tv.setBackgroundColor(Color.WHITE)
        } else {
            if (item.parkUser == 0L) {
                tv.setBackgroundColor(Color.parseColor("#8cc703"))
            } else {
                tv.setBackgroundColor(Color.parseColor("#cccccc"))
            }
        }
        tv.setText(item.parkName)
    }
}