package com.bawei.park.ui.login

import com.bawei.park.ApiService
import com.bawei.park.entity.LoginEntity
import com.mooc.lib_network.HttpType
import com.mooc.lib_network.NetWorkFactory
import com.mooc.lib_network.entity.BaseRepository
import com.mooc.lib_network.entity.NetResult

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 13:03
 * Description: This is LoginRepo
 */
class LoginRepo : BaseRepository() {
    //转化后的登录方法

    suspend fun login(str : String): NetResult<LoginEntity> {
        return requestCall ( call={requestLogin(str)} )
    }

    //登录方法
    suspend fun requestLogin(str : String):NetResult<LoginEntity>{
        return handlerResponse(
            NetWorkFactory.factory(HttpType.SIGN)
                .getRetrofit().create(ApiService::class.java)
                .requestLogin(createBody(str))
        )
    }
}