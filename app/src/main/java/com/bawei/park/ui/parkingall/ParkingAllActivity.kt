package com.bawei.park.ui.parkingall

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.bawei.park.App
import com.bawei.park.R
import com.bawei.park.databinding.ActivityParkingallBinding

import com.blankj.utilcode.util.LogUtils
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemChildClickListener
import com.mooc.lib_common.mvvm.BaseActivity
import com.yanyusong.y_divideritemdecoration.Y_DividerItemDecoration


/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/2
 * Time: 8:25
 * Description: This is ParkingAllActivity
 */
class ParkingAllActivity : BaseActivity<ParkingAllViewModel, ActivityParkingallBinding>()
    ,OnItemChildClickListener {

    var parkAdapter : ParkingAllAdapter = ParkingAllAdapter()
    val y_d : Y_DividerItemDecoration = ParkDividerItemDecoration(App.context)

    override fun initView() {
        var manager = GridLayoutManager(this,5)
        manager.orientation = GridLayoutManager.VERTICAL
        v.rec.layoutManager = manager
        v.rec.adapter = parkAdapter
        v.rec.addItemDecoration(y_d)
        parkAdapter.setOnItemChildClickListener(this)
    }

    override fun bindLayout(): Int {
        return R.layout.activity_parkingall
    }

    override fun initData() {
        vm.data.observe(this,{
            var index = 0
            while (index < it.size) {
                LogUtils.json(it.get(index).toString())
                parkAdapter.setNewInstance(it)
                index++
            }
        })
        vm.requestPark()
    }

    override fun onItemChildClick(adapter: BaseQuickAdapter<*, *>, view: View, position: Int) {
        startActivity(Intent(this,PreViewActivity::class.java))
//        ToastUtils.showLong("ok")
    }

    /**
     * onCreate
     * onStart
     * onResume
     * onPouse
     * onStop
     * onDestory
     * onRestart
     */
}