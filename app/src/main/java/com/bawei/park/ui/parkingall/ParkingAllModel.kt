package com.bawei.park.ui.parkingall

import com.bawei.park.ApiService
import com.mooc.lib_network.HttpType
import com.mooc.lib_network.NetWorkFactory
import com.mooc.lib_network.entity.BaseRepository
import com.mooc.lib_network.entity.NetResult

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/1
 * Time: 18:40
 * Description: This is ParkingAllModel
 */
class ParkingAllModel : BaseRepository() {
    suspend fun parkingAll() : NetResult<ArrayList<ParkingAllEntity>> {
        return requestCall (call = {requestParking()})
    }
    suspend fun requestParking() : NetResult<ArrayList<ParkingAllEntity>> {
        return handlerResponse(
            NetWorkFactory.factory(HttpType.TOKEN)
                .getRetrofit().create(ApiService::class.java)
                .requestParkingAll()
        )
    }
}