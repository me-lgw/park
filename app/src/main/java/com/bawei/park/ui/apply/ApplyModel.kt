package com.bawei.park.ui.apply

import com.bawei.park.ApiService
import com.mooc.lib_network.HttpType
import com.mooc.lib_network.NetWorkFactory
import com.mooc.lib_network.entity.BaseRepository
import com.mooc.lib_network.entity.NetResult

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/7
 * Time: 10:55
 * Description: This is ApplyModel
 */
class ApplyModel : BaseRepository() {

    suspend fun apply() : NetResult<ArrayList<ApplyEntity>> {
        return requestCall ( call = {requestApply()} )
    }

    suspend fun requestApply() : NetResult<ArrayList<ApplyEntity>> {
        return handlerResponse(
            NetWorkFactory.factory(HttpType.TOKEN).getRetrofit()
                .create(ApiService::class.java)
                .requestApply()
        )
    }
}