package com.bawei.park.ui.parkingall

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/1
 * Time: 16:15
 * Description: This is ParkingAllEntity
 */
class ParkingAllEntity {
    var parkUser : Long = 0
    var parkId : Long = 0
    var parkTime: String? = null
    var parkName: String? = null
    var parkEnd: String? = null
    var parkStart: String? = null
    var type = 0//区分item显示样式第一行、已选中、未选中

    override fun toString(): String {
        return parkName+parkUser+type
    }
}


