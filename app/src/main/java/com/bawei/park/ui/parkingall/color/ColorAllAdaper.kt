package com.bawei.park.ui.parkingall.color

import android.graphics.Color
import android.widget.ImageView
import com.bawei.park.R
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/4
 * Time: 14:11
 * Description: This is ColorAllAdaper
 *

 */
class ColorAllAdaper : BaseQuickAdapter<ColorAllEntity,
        BaseViewHolder>(R.layout.item_colorall) {

    init {
        addChildClickViewIds(R.id.colorAll)
    }

    override fun convert(holder: BaseViewHolder, item: ColorAllEntity) {
        var iv = holder.getView<ImageView>(R.id.colorAll)
        iv.setBackgroundColor(Color.parseColor(item.colorValue))
    }
}