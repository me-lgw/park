package com.bawei.park.ui.parkingall.car

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/5
 * Time: 11:21
 * Description: This is CarImgEntity
 */
class CarImgEntity {
    /**
     * "carId": 0, \
    "carImgId": 0, \
    "imgPath": "string" \
     */
    var carId : Long = 0
    var carImgId : Long = 0
    var imgPath :String? = null
}