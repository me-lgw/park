package com.bawei.park.ui.parkingall.color

import android.util.Log
import com.bawei.park.ApiService
import com.mooc.lib_network.HttpType
import com.mooc.lib_network.NetWorkFactory
import com.mooc.lib_network.entity.BaseRepository
import com.mooc.lib_network.entity.NetResult

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/3
 * Time: 16:32
 * Description: This is ColorAllModel
 */
class ColorAllModel : BaseRepository() {

    suspend fun colorAll() : NetResult<ArrayList<ColorAllEntity>> {
        return requestCall (call = {requestColorAll()})
    }


    suspend fun requestColorAll() : NetResult<ArrayList<ColorAllEntity>> {
        return handlerResponse(
            NetWorkFactory.factory(HttpType.TOKEN).getRetrofit()
                .create(ApiService::class.java)
                .requestColorAll()
        )
    }
}