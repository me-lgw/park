package com.bawei.park.ui.login

import android.content.Intent
import androidx.lifecycle.Observer

import com.bawei.park.BR

import com.bawei.park.R
import com.bawei.park.databinding.ActivityLoginBinding
import com.bawei.park.ui.home.MainActivity
import com.mooc.lib_common.mvvm.BaseActivity

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 11:42
 * Description: This is LoginActivity
 */

class LoginActivity : BaseActivity<LoginViewModel,ActivityLoginBinding>(),Observer<String>{

    override fun initView() {

    }

    override fun bindLayout(): Int {
        return R.layout.activity_login
    }

    override fun initData() {
        vm.loginBus.observe(this,this)
        v.setVariable(BR.vm,vm)
    }

    override fun onChanged(t: String?) {
        var intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}