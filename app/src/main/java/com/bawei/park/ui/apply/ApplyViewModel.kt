package com.bawei.park.ui.apply

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mooc.lib_common.bus.LiveDataBus
import com.mooc.lib_network.entity.NetResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/3/7
 * Time: 10:56
 * Description: This is ApplyViewModel
 */
class ApplyViewModel : ViewModel() {

    var model = ApplyModel()
    var data = LiveDataBus<ArrayList<ApplyEntity>>()

    fun requestApply() {
        viewModelScope.launch(Dispatchers.IO) {
            var apply = model.apply()
            if (apply is NetResult.Success) {
                var list = apply.data
                data.postValue(list)
            }
        }
    }
}