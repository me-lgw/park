package com.bawei.park

import android.content.Context
import com.mooc.lib_common.CommonApp

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 14:29
 * Description: This is App
 */
class App : CommonApp() {

    companion object {
        lateinit var context: Context

    }

    override fun onCreate() {
        super.onCreate()
        context = baseContext
    }
}