package com.bawei.park

import com.bawei.park.entity.LoginEntity
import com.bawei.park.ui.apply.ApplyEntity
import com.bawei.park.ui.parkingall.ParkingAllEntity
import com.bawei.park.ui.parkingall.car.CarImgEntity
import com.bawei.park.ui.parkingall.color.ColorAllEntity
import com.mooc.lib_network.entity.BaseEntity
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 13:22
 * Description: This is ApiService
 */
interface ApiService {
    //用户登录接口
    @POST("sysUser/loginUser")
    suspend fun requestLogin(@Body body: RequestBody): BaseEntity<LoginEntity>

    //全部车辆
    @POST("sysParking/selParking")
    suspend fun requestParkingAll() : BaseEntity<ArrayList<ParkingAllEntity>>

    //全部颜色
    @POST("sysColor/selColor")
    suspend fun requestColorAll() : BaseEntity<ArrayList<ColorAllEntity>>

    //添加车辆图片
    @POST("sysCar/addCarImg")
    suspend fun requestCarImg() : BaseEntity<ArrayList<CarImgEntity>>

    @POST("sysApply/selAllApply")
    suspend fun requestApply() : BaseEntity<ArrayList<ApplyEntity>>
}