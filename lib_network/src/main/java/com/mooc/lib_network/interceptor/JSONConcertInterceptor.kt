package com.mooc.lib_network.interceptor

import android.util.Log
import com.google.gson.Gson
import com.mooc.lib_network.entity.ConvertEntity
import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import kotlin.math.log

class JSONConcertInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var response = chain.proceed(chain.request())
        //response body中得json下得\"去掉
        var body = response.body()
        var str = body!!.string()
        val map = java.util.HashMap<String,Any>()
        val valuesMap = java.util.HashMap<String,Any>()
        val convertEntity = Gson().fromJson(str,ConvertEntity::class.java)
        var values = convertEntity.values
        var jsonStr = ""
        if (convertEntity.values.indexOf("[") == 0) {
            var list = ArrayList<HashMap<String,Any>>()
            var jay = JSONArray(values)
            var index = 0
            while (index < jay.length()) {
                var hashMap = HashMap<String,Any>()
                var job = jay.getJSONObject(index)
                val it = job.keys()
                while (it.hasNext()){
                    var key : String = it.next() as String
                    hashMap.put(key,job.get(key))
                }
                list.add(hashMap)
                index++
            }
            map["values"] = list
        } else {
            val job = JSONObject(values)
            var it  = job.keys()
            while (it.hasNext()){
                var key : String = it.next() as String
//            valuesMap[key] = job.get(key)
                valuesMap.put(key,job.get(key))
            }
            map["values"] = valuesMap
        }
        map["statuesCode"] = convertEntity.statuesCode
        map["msg"] = convertEntity.msg
        map["home"] = convertEntity.home
//        map["values"] = valuesMap
        jsonStr = Gson().toJson(map).toString()
        Log.e("lgw", "jsonStr:"+jsonStr)

        //去除转义字符后得body
        var newBody : ResponseBody = ResponseBody.create(
                MediaType.parse("application/json"),jsonStr)
        var builder = Response.Builder()
        builder.request(chain.request())
        builder.code(200)
        builder.message(response.message())
        builder.protocol(response.protocol())//http1.1
        builder.body(newBody)//响应体
        return builder.build()
    }
}