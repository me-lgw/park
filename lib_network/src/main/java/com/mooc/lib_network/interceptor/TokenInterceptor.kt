package com.mooc.lib_network.interceptor

import com.blankj.utilcode.util.SPUtils
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class TokenInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder : Request.Builder = chain.request().newBuilder()
        builder.header("token",SPUtils.getInstance().getString("token"))
        val request = builder.build()
        return chain.proceed(request)
    }
}