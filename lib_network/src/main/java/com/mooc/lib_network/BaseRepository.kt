package com.mooc.lib_network.entity

import kotlinx.coroutines.coroutineScope
import okhttp3.MediaType
import okhttp3.RequestBody

/**
 * 全部网络请求基类->相当于MVVM中的Model
 * */
open class BaseRepository {

    suspend fun<T:Any> requestCall(
        call: suspend () -> NetResult<T>
    ):NetResult<T>{
        return call()
    }

    suspend fun <T : Any>handlerResponse(
        baseEntity : BaseEntity<T>
    ): NetResult<T> {
        return coroutineScope{
            if (baseEntity.statuesCode == 200){
                //请求成功
                NetResult.Success(baseEntity.values)
            }else{
                //请求失败
                NetResult.Erro("请求失败")
            }
        }
    }

    fun createBody(str : String): RequestBody {
        return RequestBody.create(MediaType.parse("application/json"),str)
    }

}