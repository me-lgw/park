package com.mooc.lib_network.entity

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 13:25
 * Description: This is LoginEntity
 */
data class LoginEntity(val pId : Long,val time : String,val token : String,
                       val uId : Long,val uName:String,val uPwd:String,
                       val dept_id : Long) {
}