package com.mooc.lib_network.entity
/**
 * 数据实体基类
 * Retrofit获取json以及使用gson映射返回的实体类
 * T:具体json中的data对应的实体类或者集合(ArrayList)
 * */
data class BaseEntity<out T> (val statuesCode : Int,
                              val msg : String,
                              val values : T,val home:String)