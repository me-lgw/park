package com.mooc.lib_network.entity
/**
 * VM层使用数据封装
 * 请求成功result
 * 请求失败result
 * */
open class NetResult<out T:Any> {

    data class Success<out T:Any>(val data: T) : NetResult<T>()

    data class Erro (val erroMsg : String) : NetResult<Nothing>()

}