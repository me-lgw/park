package com.mooc.lib_network

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 13:08
 * Description: This is NetWorkFactory
 */
class NetWorkFactory {
    companion object{
        var tokenRetrofit:RetrofitImpl?=null
        var signRetrofit:RetrofitImpl?=null
        var tokensignRetrofit:RetrofitImpl?=null
        var uploadRetrofit:RetrofitImpl?=null
        fun factory(type: HttpType):RetrofitImpl{
            var retrofitImpl:RetrofitImpl?=null
            when(type){
                HttpType.TOKEN->{
                    if (tokenRetrofit==null)
                        tokenRetrofit = TokenRetrofit()
                    retrofitImpl = tokenRetrofit
                }
                HttpType.SIGN->{
                    if (signRetrofit == null)
                        signRetrofit = SignRetrofit()
                    retrofitImpl = signRetrofit
                }
                HttpType.TOKENSIGN->{
                    if (tokensignRetrofit == null)
                        tokensignRetrofit = TokenSignRetrofit()
                    retrofitImpl = tokensignRetrofit
                }
                HttpType.UPLOAD->{
                    if(uploadRetrofit==null)
                        uploadRetrofit = UploadRetrofit()
                    retrofitImpl = uploadRetrofit
                }
            }
            return retrofitImpl!!
        }
    }
}