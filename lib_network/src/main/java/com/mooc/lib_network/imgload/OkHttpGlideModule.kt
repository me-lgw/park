package com.mooc.lib_network.imgload

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.module.AppGlideModule
import okhttp3.OkHttpClient
import java.io.InputStream
import java.util.concurrent.TimeUnit

/**
 * 自定义组件通过replace方式替换掉glide原有加载图片的
 * 网络请求类HttpUrlConnection
 * */
@GlideModule
class OkHttpGlideModule : AppGlideModule(){
    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        super.registerComponents(context, glide, registry)
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(10,TimeUnit.SECONDS)
            .readTimeout(60,TimeUnit.SECONDS)
            .writeTimeout(10,TimeUnit.SECONDS)
        registry.replace(GlideUrl::class.java,InputStream::class.java, OkHttpUrlLoader.Factory(builder.build()))
    }

}