package com.mooc.lib_network.imgload

import android.widget.ImageView
import com.bumptech.glide.module.AppGlideModule
import com.mooc.lib_network.R

/**
 * 整合完成的图片加载框架
 * 不适用等比列缩放原因:100*100 图片宽高200*100 ->100*50 裁切:100*100
 * */
class ImageLoad {

    companion object{

        fun loadMatch(urlPath : String ,img :ImageView){
            var path = ""
            if (null!=urlPath) {
                path = urlPath
            }
            GlideApp.with(img.context)
                .load(path)
                .centerCrop()
                .placeholder(R.drawable.icon_god_comment2)
                .error(R.drawable.icon_close)
                .into(img)
        }

    }

}