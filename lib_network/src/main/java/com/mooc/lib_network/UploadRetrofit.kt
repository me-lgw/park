package com.mooc.lib_network

import com.mooc.lib_network.interceptor.JSONConcertInterceptor
import com.mooc.lib_network.interceptor.SignInterceptor
import com.mooc.lib_network.interceptor.TokenInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class UploadRetrofit  :RetrofitImpl {
    private lateinit var retrofit: Retrofit
    init {
        var okBuilder = OkHttpClient.Builder()
        okBuilder.writeTimeout(3, TimeUnit.MINUTES)
        okBuilder.readTimeout(3, TimeUnit.MINUTES)
        okBuilder.connectTimeout(3, TimeUnit.MINUTES)
        okBuilder.addInterceptor(JSONConcertInterceptor())
        okBuilder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        var builder = Retrofit.Builder()
        builder.baseUrl(ApiConst.BASEURL)
        builder.client(okBuilder.build())
        builder.addConverterFactory(GsonConverterFactory.create())
        retrofit = builder.build()
    }
    override fun getRetrofit(): Retrofit {
        return retrofit
    }
}