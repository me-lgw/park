package com.mooc.lib_network

import com.blankj.utilcode.util.EncryptUtils

class MD5Utils {

    companion object{
        fun createSign(str: String):String{
            //32位大写的md5加密
            val signStr: String = EncryptUtils.encryptMD5ToString(str + "tamboo")
            return signStr.toLowerCase()
        }
    }

}