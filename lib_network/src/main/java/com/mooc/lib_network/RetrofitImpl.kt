package com.mooc.lib_network

import retrofit2.Retrofit

interface RetrofitImpl {

    fun getRetrofit():Retrofit

}