package com.mooc.lib_network

/**
 * Created by Android Studio.
 * User: lenovo
 * Date: 2022/2/25
 * Time: 13:09
 * Description: This is HttpType
 */
enum class HttpType {
    TOKEN,SIGN,TOKENSIGN,UPLOAD
}