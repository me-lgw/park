package com.mooc.lib_common.arouter.service

import com.alibaba.android.arouter.facade.template.IProvider
import com.mooc.lib_common.model.UserEntity

interface UserLoginAService : IProvider {

    fun isLogin():Boolean

    fun getUser():UserEntity?

}