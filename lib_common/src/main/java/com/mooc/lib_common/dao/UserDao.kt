package com.mooc.lib_common.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.mooc.lib_common.model.UserEntity

/**
 * 用户数据操作
 * */
@Dao
interface UserDao {

    //添加数据
    @Insert
    suspend fun insertUser(user : UserEntity)
    //查询数据
    @Query("Select * from userentity")
    suspend fun queryUser():List<UserEntity>
    //删除数据
    @Query("Delete from userentity")
    suspend fun delAllUser()
    //修改数据
    @Update
    suspend fun updateUser(user : UserEntity)


}