package com.mooc.lib_common.dao

import androidx.room.Database
import androidx.room.RoomDatabase
import com.mooc.lib_common.model.UserEntity

@Database(entities = arrayOf(UserEntity::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}