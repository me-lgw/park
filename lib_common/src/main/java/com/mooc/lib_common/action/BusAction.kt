package com.mooc.lib_common.action

data class BusAction(val action:String, val data: HashMap<String, Any>?)
