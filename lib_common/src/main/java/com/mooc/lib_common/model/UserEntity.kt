package com.mooc.lib_common.model

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 *  "id": 1260,
"userId": 1642558754,
"name": "1",
"avatar": "1",
"description": "1",
"likeCount": 0,
"topCommentCount": 0,
"followCount": 0,
"followerCount": 0,
"qqOpenId": "1",
"expires_time": 1,
"score": 0,
"historyCount": 0,
"commentCount": 0,
"favoriteCount": 0,
"feedCount": 0,
"hasFollow": false
 * */
@Entity
data class UserEntity(@PrimaryKey val id:Int, val userId : Int, val name : String, val avatar : String,
                      var description:String, val likeCount:Int, val topCommentCount:Int,
                      val followCount:Int, val followerCount:Int,
                      val qqOpenId:String, val expires_time:Long, val score:Int,
                      val historyCount:Int, val commentCount:Int,
                      val favoriteCount:Int, val feedCount:Int, val hasFollow:Boolean)
