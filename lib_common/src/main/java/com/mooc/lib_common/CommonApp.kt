package com.mooc.lib_common

import android.content.ComponentCallbacks2
import androidx.room.Room
import com.bumptech.glide.Glide
import com.mooc.lib_base_application.BaseApplication
import com.mooc.lib_common.dao.AppDatabase
import com.mooc.lib_common.model.UserEntity

open class CommonApp : BaseApplication() {

    companion object{
        lateinit var db : AppDatabase
        var userEntity: UserEntity?= null
    }

    override fun onCreate() {
        super.onCreate()
        //载入ijk.so->载入解码库
        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "ppjock"
        ).build()
    }

    override fun onTrimMemory(level: Int) {
        super.onTrimMemory(level)
        if (level == ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW){
            //清理glide占用内存
            Glide.get(this).clearMemory()
        }

    }

}