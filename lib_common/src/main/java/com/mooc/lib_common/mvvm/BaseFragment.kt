package com.mooc.lib_common.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mooc.lib_base_application.BaseApplication
import org.koin.androidx.viewmodel.ext.android.getViewModel
import java.lang.reflect.ParameterizedType
import kotlin.reflect.KClass

abstract class BaseFragment<VM : ViewModel,V : ViewDataBinding> : Fragment(){

    protected lateinit var vm : VM
    protected lateinit var v : V

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        BaseApplication.instance!!.ref.watch(this)
        v = DataBindingUtil.inflate(inflater,bindLayout(),container,false)
        initViewModel()
        initView()
        initData()
        return v.root
    }

    //创建ViewModel对象
    fun initViewModel(){
        val parameterizedType = this.javaClass.genericSuperclass as ParameterizedType
        val types = parameterizedType.actualTypeArguments
        val clazz : Class<VM> = types[0] as Class<VM>
        vm = ViewModelProvider(this,
            ViewModelProvider.NewInstanceFactory()).get(clazz)
    }

    //初始化控件
    abstract fun initView()
    //创建databinding对象
    abstract fun bindLayout():Int
    //初始化数据
    abstract fun initData()

    override fun onDestroy() {
        super.onDestroy()
        v.unbind()
    }

}