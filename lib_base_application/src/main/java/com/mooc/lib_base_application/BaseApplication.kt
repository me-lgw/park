package com.mooc.lib_base_application

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import com.squareup.leakcanary.LeakCanary
import com.squareup.leakcanary.RefWatcher

/**
 * 多模块打包合并时,清单文件下相同属性会产生覆盖
 * */
open class BaseApplication : Application(){

    lateinit var ref : RefWatcher

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        //初始化LeakCanary->如何检测
        /**
         * 强引用:直接new对象->内部不够抛出oom
         * 软引用:->被软引用标签包裹的对象，内存不够清理内存时会被清理掉
         * 弱引用:->被弱引用标签包裹的对象，一清理内存就会被清理掉
         * 虚引用->清理内存时单独提供一个queue队列用于保存要被清理的对象
         * 使用虚引用进行应用要检测泄露的对象，当清理完成判断队列中对象是否被清理掉
         * 清理掉->正常;为被清理掉->泄露
         * */
        ref = LeakCanary.install(this)
    }

    companion object{
        var instance:BaseApplication?=null
    }

}